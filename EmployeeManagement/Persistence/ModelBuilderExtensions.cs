﻿using Microsoft.EntityFrameworkCore;
using EmployeeManagement.Core.Models;

namespace EmployeeManagement.Persistence
{
    public static class ModelBuilderExtensions
    {
        public static void SeedData(this ModelBuilder builder)
        {
            builder.Entity<Employee>().HasData(
                
                new Employee
                {
                    Id = 1,
                    Name = "John",
                    Department = "Marketing",
                    Email = "cooljonny@gmail.com",
                    Position = "Manager",
                    Age = 34,
                    PhotoPath = "seed_8a32b60a-7374-42fb-b87b-ff29438412b4_image1.jpg"
                },

                new Employee
                {
                    Id = 2,
                    Name = "Tom",
                    Department = "Marketing",
                    Email = "tombrown@yahoo.com",
                    Position = "Manager",
                    Age = 27,
                    PhotoPath = "seed_2a36223f-ee7f-4fd3-afc9-35be755adc47_image2.jpg"
                },

                new Employee
                {
                    Id = 3,
                    Name = "Anna",
                    Department = "Accounting",
                    Email = "annawhite23@example.com",
                    Position = "Accountant",
                    Age = 29,
                    PhotoPath = "seed_eccb94af-f032-459a-a6a6-1dc55ca2f8a7_image3.jpg"
                });
        }

    }
}
