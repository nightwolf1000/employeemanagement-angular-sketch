﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Core.Interfaces;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EmployeeManagement.Persistence
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _context;

        public EFEmployeeRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        
        public async Task<IEnumerable<Employee>> GetAllEmployeesAsync()
        {
            return await _context.Employees.AsNoTracking().ToListAsync();   
        }

        public async Task<Employee> GetEmployee(int id)
        {
            return await _context.Employees.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Employee> AddEmployeeAsync(Employee employee)
        {
            await _context.AddAsync(employee);
            await _context.SaveChangesAsync();
            return employee;
        }

        public async Task<Employee> UpdateEmployeeAsync(Employee employeeChanges)
        {
            EntityEntry<Employee> dbEntry = _context.Employees.Attach(employeeChanges);
            dbEntry.State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return employeeChanges;
        }

        public async Task<Employee> DeleteEmployeeAsync(Employee employee)
        {
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return employee;
        }
    }
}
