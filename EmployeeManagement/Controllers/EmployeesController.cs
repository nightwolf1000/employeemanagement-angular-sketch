﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Core.Interfaces;
using EmployeeManagement.ApplicationServices;
using EmployeeManagement.Controllers.Resources;

namespace EmployeeManagement.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        private readonly IEmployeeRepository _repository;
        private readonly EmployeeResourceService _resourceService;

        public EmployeesController(IEmployeeRepository repository, EmployeeResourceService resourceService)
        {
            this._repository = repository;
            this._resourceService = resourceService;
        }

        // GET: api/employees
        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            var employees = await _repository.GetAllEmployeesAsync();
            return new ObjectResult(employees);
        }

        // GET: api/employees/2
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployee([FromRoute] int id)
        {
            Employee employee = await _repository.GetEmployee(id);

            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        // POST: api/employees
        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromForm] EmployeeResource employeeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employee employee = new Employee();
            await _resourceService.ProcessUploadedResourceAsync(employee, employeeResource);
            Employee addedEmployee = await _repository.AddEmployeeAsync(employee);
            return Ok(addedEmployee);
        }

        // PUT: api/emloyees/2
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee([FromRoute] int id, [FromForm] EmployeeResource employeeResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Employee employee = await _repository.GetEmployee(id);

            if (employee == null)
            {
                return NotFound();
            }

            await _resourceService.ProcessUploadedResourceAsync(employee, employeeResource);
            Employee updatedEmployee = await _repository.UpdateEmployeeAsync(employee);
            return Ok(updatedEmployee);
        }

        // DELETE: api/employees/2
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee([FromRoute] int id)
        {
            Employee employee = await _repository.GetEmployee(id);

            if (employee == null)
            {
                return NotFound();
            }

            Employee deletedEmployee = await _repository.DeleteEmployeeAsync(employee);
            _resourceService.DeleteExistingPhoto(deletedEmployee);
            return Ok(deletedEmployee);
        }

    }

}
