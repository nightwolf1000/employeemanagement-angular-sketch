﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Infrastructure
{
    public static class HostExtensions
    {
        public static IHost MigrateDatabase<T>(this IHost host) where T : DbContext
        {
            using (IServiceScope scope = host.Services.CreateScope())
            {
                T context = scope.ServiceProvider.GetRequiredService<T>();
                context.Database.Migrate();
            }
            return host;
        }
    }
}
