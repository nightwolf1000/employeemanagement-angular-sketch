﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using EmployeeManagement.Core.Models;
using EmployeeManagement.Controllers.Resources;
using System.IO;

namespace EmployeeManagement.ApplicationServices
{
    public class EmployeeResourceService
    {
        private readonly IWebHostEnvironment _environment;

        public EmployeeResourceService(IWebHostEnvironment environment)
        {
            this._environment = environment;
        }
        
        public async Task ProcessUploadedResourceAsync(Employee employee, EmployeeResource employeeResource)
        {
            employee.Name = employeeResource.Name;
            employee.Email = employeeResource.Email;
            employee.Department = employeeResource.Department;
            employee.Position = employeeResource.Position;
            employee.Age = employeeResource.Age;

            if (employeeResource.Photo != null)
            {
                DeleteExistingPhoto(employee);
                employee.PhotoPath = await ProcessUploadedFileAsync(employeeResource);
            }                     
        }
        
        public void DeleteExistingPhoto(Employee employee)
        {
            if (employee.PhotoPath != null && !employee.PhotoPath.StartsWith("seed"))
            {
                string filePath = Path.Combine(_environment.WebRootPath, "images", employee.PhotoPath);
                System.IO.File.Delete(filePath);
            }
        }

        private async Task<string> ProcessUploadedFileAsync(EmployeeResource employeeResource)
        {
            string uploadsFolder = Path.Combine(_environment.WebRootPath, "images");
            string uniqueFileName = Guid.NewGuid().ToString() + "_" + employeeResource.Photo.FileName;
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await employeeResource.Photo.CopyToAsync(fileStream);
            }

            return uniqueFileName;
        }
    }
}
