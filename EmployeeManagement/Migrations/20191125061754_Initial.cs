﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmployeeManagement.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Email = table.Column<string>(maxLength: 255, nullable: false),
                    Department = table.Column<string>(maxLength: 255, nullable: false),
                    Position = table.Column<string>(maxLength: 255, nullable: false),
                    Age = table.Column<int>(nullable: false),
                    PhotoPath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Age", "Department", "Email", "Name", "PhotoPath", "Position" },
                values: new object[] { 1, 34, "Marketing", "cooljonny@gmail.com", "John", "seed_8a32b60a-7374-42fb-b87b-ff29438412b4_image1.jpg", "Manager" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Age", "Department", "Email", "Name", "PhotoPath", "Position" },
                values: new object[] { 2, 27, "Marketing", "tombrown@yahoo.com", "Tom", "seed_2a36223f-ee7f-4fd3-afc9-35be755adc47_image2.jpg", "Manager" });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Age", "Department", "Email", "Name", "PhotoPath", "Position" },
                values: new object[] { 3, 29, "Accounting", "annawhite23@example.com", "Anna", "seed_eccb94af-f032-459a-a6a6-1dc55ca2f8a7_image3.jpg", "Accountant" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");
        }
    }
}
