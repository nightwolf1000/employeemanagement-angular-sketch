import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeCreateEditComponent } from './employee-create-edit/employee-create-edit.component';

const routes: Routes = [
    { path: "", component: EmployeesComponent, pathMatch: "full" },
    { path: "employee/details/:id", component: EmployeeDetailsComponent },
    { path: "create-employee", component: EmployeeCreateEditComponent },
    { path: "employee/edit/:id", component: EmployeeCreateEditComponent },
    { path: "**", redirectTo: "/" }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
