import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { HttpClient } from 'selenium-webdriver/http';
import { EmployeeService } from './services/employee.service';
import { FileValidationService } from './services/file-validation-service.service';
import { ConfirmDeletionService } from './services/confirm-deletion-service.service';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { EmployeeCreateEditComponent } from './employee-create-edit/employee-create-edit.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    EmployeeDetailsComponent,    
    NavMenuComponent,
    EmployeeCreateEditComponent,
    ConfirmDialogComponent,
    LoginComponent,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule],
  entryComponents: [ConfirmDialogComponent],
  providers: [EmployeeService, FileValidationService, ConfirmDeletionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
