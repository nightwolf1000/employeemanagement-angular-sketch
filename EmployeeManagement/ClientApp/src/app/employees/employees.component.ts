import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee';
import { EmployeeService } from '../services/employee.service';
import { ConfirmDeletionService } from '../services/confirm-deletion-service.service';
 
@Component({
    selector: 'app-employees',
    templateUrl: './employees.component.html',
    styleUrls: ['./employees.component.css']    
})
export class EmployeesComponent implements OnInit {

    employees: Employee[];

    constructor(
        private _employeeService: EmployeeService,
        private _confirmDeletionService: ConfirmDeletionService, ) { }

    ngOnInit() {
        this.loadEmployees();
    }

    loadEmployees() {
        this._employeeService.getEmployees()
            .subscribe((data: Employee[]) => this.employees = data);
    }    

    delete(employee: Employee): void {
        const title = "Confirm Deletion";
        const message = `Are you sure you want to delete
          employee ${employee.name} with id=${employee.id}?`;

        this._confirmDeletionService.openDialog(title, message)
            .subscribe(dialogResult => {
                if (dialogResult) {
                    this._employeeService.deleteEmployee(employee.id)
                        .subscribe(data => this.loadEmployees());
                }
            });
    }
}
