import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { ConfirmDeletionService } from '../services/confirm-deletion-service.service';
import { Employee } from '../models/employee';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

    employee: Employee;
    employeeId: number;   

    constructor(
        private _employeeService: EmployeeService,
        private _confirmDeletionService: ConfirmDeletionService,
        private _avRoute: ActivatedRoute,
        private _router: Router) {      
        if (this._avRoute.snapshot.params["id"]) {
            this.employeeId = this._avRoute.snapshot.params["id"];
      }
    }

    ngOnInit() {
        this.loadEmployee();
    }

    loadEmployee() {
        this._employeeService.getEmployee(this.employeeId)
            .subscribe((data: Employee) => this.employee = data);
    }

    delete(employee: Employee): void {
        const title = "Confirm Deletion";
        const message = `Are you sure you want to delete
          employee ${employee.name} with id=${employee.id}?`;

        this._confirmDeletionService.openDialog(title, message)
            .subscribe(dialogResult => {
                if (dialogResult) {
                    this._employeeService.deleteEmployee(employee.id)
                        .subscribe(data => this._router.navigate(["/"]));
                }     
            });         
    }
}
