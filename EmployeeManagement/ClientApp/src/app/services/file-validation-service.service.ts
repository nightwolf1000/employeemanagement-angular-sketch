import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileValidationService {

    private allowedFileTypes: Array<string> = ["image/png", "image/jpeg"];
    private maxFileSize: number = 1048576;

    validateFileInput(file: File) {   
        if (!this.allowedFileTypes.includes(file.type)) {
            return { isValid: false, errorMessage: "This file extension is not allowed!"};            
        }
        else if (file.size > this.maxFileSize) {
            return { isValid: false, errorMessage: "Maximum allowed file size is 1048576 bytes." };            
        }
        else {
            return { isValid: true, errorMessage: null };
        }        
    }
}
