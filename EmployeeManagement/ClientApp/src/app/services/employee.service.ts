import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    private myApiUrl: string = "/api/employees"

    constructor(private _http: HttpClient) {
    }

    getEmployees() {
        return this._http.get(this.myApiUrl);
    }

    getEmployee(id: number) {
        return this._http.get(this.myApiUrl + "/" + id);
    }

    saveEmployee(employee: Employee, photoToUpload: File) {
        let formData = this.createFormData(employee, photoToUpload);

        if (!employee.id) {
            return this._http.post(this.myApiUrl, formData);
        }
        else {
            return this._http.put(this.myApiUrl + "/" + employee.id, formData);
        }
    }    

    deleteEmployee(id: number) {
        return this._http.delete(this.myApiUrl + "/" + id);
    }

    private createFormData(employee: Employee, photoToUpload: File) : FormData {
        let formData: FormData = new FormData();

        formData.append("name", employee.name);
        formData.append("email", employee.email);
        formData.append("department", employee.department);
        formData.append("position", employee.position);
        formData.append("age", employee.age.toString());
        formData.append("photo", photoToUpload);

        return formData;
    }
}
