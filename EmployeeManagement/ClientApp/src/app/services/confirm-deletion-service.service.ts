import { Injectable } from '@angular/core';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeletionService {    
    constructor(private _dialog: MatDialog, ) {
    }

    openDialog(title: string, message: string) {

        const dialogData = new ConfirmDialogModel(title, message);
        const dialogRef = this._dialog.open(ConfirmDialogComponent, {
            position: { top: '20px' },
            autoFocus: false,
            height: "205px",
            width: "300px",            
            data: dialogData
        });

        return dialogRef.afterClosed();        
    }
}
