import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { FileValidationService } from '../services/file-validation-service.service';
import { Employee } from '../models/employee';

@Component({
  selector: 'app-employee-create-edit',
  templateUrl: './employee-create-edit.component.html',
  styleUrls: ['./employee-create-edit.component.css']
})
export class EmployeeCreateEditComponent implements OnInit {

    employee: Employee = new Employee();
    employeeId: number;
    photoToUpload: File;
    fileValidationResult: any;
    newPhoto: any;

    constructor(
        private _employeeService: EmployeeService,
        private _validationService: FileValidationService,
        private _avRoute: ActivatedRoute,
        private _router: Router) {        
    }

    ngOnInit() {
        if (this._avRoute.snapshot.params["id"]) {
            this.employeeId = this._avRoute.snapshot.params["id"];
            this.loadEmployee();
        }        
    }

    loadEmployee() {
        this._employeeService.getEmployee(this.employeeId)
            .subscribe((data: Employee) => this.employee = data);
    }   

    saveEmployee() {
        this._employeeService.saveEmployee(this.employee, this.photoToUpload)
            .subscribe((data: Employee) => {
                this._router.navigate(["/employee/details", data.id]);
            });     
    }

    onFileChanged(files: FileList) {
        if (files.length > 0) {
            let file = files.item(0);
            this.fileValidationResult = this._validationService.validateFileInput(file);

            if (this.fileValidationResult.isValid) {
                this.photoToUpload = file;

                let fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                fileReader.onload = () => {
                    this.newPhoto = fileReader.result;
                };
            }
            else {
                this.photoToUpload = null;
            }
        }
    }
}
