export class Employee {
    constructor(
        public id?: number,
        public name?: string,
        public email?: string,
        public department?: string,
        public position?: string,
        public age?: number,
        public photoPath?: string
    ) { }

}
