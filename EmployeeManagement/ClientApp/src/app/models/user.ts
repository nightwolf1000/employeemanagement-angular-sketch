export class User {
    userName: string;    
    isLoggedIn: boolean;
    role: string;
}
