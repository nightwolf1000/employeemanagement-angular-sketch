﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.Core.Models
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        [Required]
        [MaxLength(255)]
        public string Email { get; set; }
        [Required]
        [MaxLength(255)]
        public string Department { get; set; }
        [Required]
        [MaxLength(255)]
        public string Position { get; set; }
        [Required]
        [Range(0, 99)]
        public int? Age { get; set; }
        public string PhotoPath { get; set; }
    }
}
